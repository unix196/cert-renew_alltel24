#!/bin/bash

#	скрипт обновляет сертификат от Lets Encrypt,
#	 проверяет - обновился ли он, 
#	отправляет инфу на почту

days_40=3456000
days_30=2592000
days_20=1728000
hostname="sip21.alltel24.ru"
email="support@alltel24.ru"
command_renew="certbot-auto  renew"

#проверить что сертификат не протухнет через 30 дней

openssl x509 -in /etc/letsencrypt/live/${hostname}/fullchain.pem -noout -checkend ${days_30}

if [ $? -eq 1 ]
then


	#Fix iptables rules for Lets Encrypt chech-servers:
	#echo -e "Разблокируем доступ: iptables -A INPUT --src 0.0.0.0/0 -p tcp -m tcp --dport {80,443} -j ACCEPT\n"
	#iptables -A INPUT --src 0.0.0.0/0 -p tcp -m tcp --dport 80 -j ACCEPT
	#iptables -A INPUT --src 0.0.0.0/0 -p tcp -m tcp --dport 443 -j ACCEPT
	echo -e "Разблокируем доступ: iptables -D INPUT --src 0.0.0.0/0 --dst 0.0.0.0/0 -j DROP\n"
	iptables -D INPUT --src 0.0.0.0/0 --dst 0.0.0.0/0 -j DROP

	#echo -e "Stopping webserver\n"
	#/etc/init.d/apache2 stop

	$command_renew

	enddate=`openssl x509 -in /etc/letsencrypt/live/${hostname}/fullchain.pem -noout -enddate`
	startdate=`openssl x509 -in /etc/letsencrypt/live/${hostname}/fullchain.pem -noout -startdate`

	echo -e "\n\nПроверяем, что сертификат не протухнет в течение следующих $days_40 (в секундах) и отправляем результат на $email\n"
	openssl x509 -in /etc/letsencrypt/live/${hostname}/fullchain.pem -noout -checkend ${days_40}

	if [ $? -eq 1 ]
	then
		echo -e " Let's Encrypt: certbot-auto  renew don't renew cert ${hostname}; \n  \
		Validity: $startdate   $enddate" | mailx -s "ERROR Renew cert for ${hostname}" $email


	else
		echo -e "Let's Encrypt: certbot-auto  renew cert ${hostname}; \n \
		Validity: $startdate   $enddate" | mailx -s "OK Renew cert for ${hostname}" $email

	fi

	#echo -e "Убираем доступ: iptables -D INPUT --src 0.0.0.0/0 -p tcp -m tcp --dport {80,443} -j ACCEPT\n"
	#iptables -D INPUT --src 0.0.0.0/0 -p tcp -m tcp --dport 80 -j ACCEPT
	#iptables -D INPUT --src 0.0.0.0/0 -p tcp -m tcp --dport 443 -j ACCEPT
	echo -e "Убираем доступ: iptables -A INPUT --src 0.0.0.0/0 --dst 0.0.0.0/0 -j DROP\n"
	iptables -A INPUT --src 0.0.0.0/0 --dst 0.0.0.0/0 -j DROP

	#echo -e "Starting nginx\n"
	#/etc/init.d/nginx start
	/etc/init.d/apache2 reload
else
	echo -e "\t\t\t\tПока время протухания сертификата letencrypt более ${days_30} секунд"

fi

